import streamlit as st
import numpy as np
import pandas as pd
#import pickle

#from pycaret.regression import *
#from pycaret.regression import load_model 

# from sklearn.ensemble import RandomForestRegressor
# from sklearn.preprocessing import LabelEncoder
from cria_modelo_precificacao import cria_modelo
from  calcula_precificacao import calc_prec

st.set_page_config(layout="wide")
# Carregando a base
base_pesados = pd.read_csv("./base_analitica_pesados.csv")

# carregando o modelo
#modelo=pickle.load(open('Precificacao_RandomForestRegressor-v1.1.pkl','rb'))
#modelo = load_model('modelo_teste_1')
modelo = cria_modelo()


# Barra lateral
#st.sidebar.title("Versões do Precificador")
#st.sidebar.image('./iveco-todos.jpg', width=300)
# st.sidebar.image('./cta_iveco2019.jpg', width=305)
st.sidebar.image('./logo_Iveco_4.png', width=250)

## Inserindo uma linha de separação entre as seções
st.sidebar.markdown('---')

st.sidebar.title("Versões do Precificador") #st.sidebar.write('Versões do Precificador')

versao_selecionada = st.sidebar.selectbox('Selecione a versão a ser utilizada', ['V0', 'V0.1'])

# Criando espaçamento para manter imagem da A3 na base
for i in range(10):
   st.sidebar.write("#")


# st.sidebar.write('Desenvolvido por Rubens Carvalho')
# st.sidebar.write('Squad Carga Pesada')

st.sidebar.image('./logo_original.png', width=200)
st.sidebar.write('Squad Carga Pesada')

if versao_selecionada ==  'V0':
   with st.container():
      st.title('Precificador para o Plano Absoluto')# - Pesados')
      # st.title('Precificador para o Plano Absoluto')# - Pesados')
      st.write("""
               Essa versão reproduz o cálculo da mesma forma como ocorre na planilha de precificação atual. \n
               """)

      KM_Anual = st.slider('Selecione a Kilometragem Anual a ser percorrida',value = 100000, min_value= 30000, max_value= 1000000, step=10000)

   col1, col2= st.columns(2)
   with col1:
      # st.header("Segmento do veículo")
      tipo_veiculo = st.radio("Segmento do veículo", 
                              ('L.A.-STRALIS 6X2','L.A.-STRALIS 6X4','L.A.-STRALIS 4X2'))

   with col2:
      missao = st.radio("Regime de uso do veículo",
                        ('Rodoviário','Severo'))
      if (tipo_veiculo == 'L.A.-STRALIS 4X2') and (missao == 'Severo'):
         st.error('Para o segmento L.A.-STRALIS 4X2, o regime Severo não é aplicado! \n\n Mostrando resultado para o regime Rodoviário.', icon="🚨")
         missao = 'Rodoviário'

   
   # Chamando a função de cálculo de precificação
  # base_pesados = pd.read_csv("./base_analitica_pesados.csv")
   df = calc_prec(base_pesados, tipo_veiculo, missao, KM_Anual)

   st.table(df)

elif versao_selecionada ==  'V0.1':
   st.title('Essa versão será disponibilizada em breve!')

   with st.container():
      st.title('Precificador para o Plano Absoluto')# - Pesados')
      # st.title('Precificador para o Plano Absoluto')# - Pesados')
      st.write("""
               Essa versão reproduz o cálculo da mesma forma como ocorre na planilha de precificação atual. \n
               """)

      KM_Anual = st.slider('Selecione a Kilometragem Anual a ser percorrida',value = 100000, min_value= 30000, max_value= 1000000, step=10000)
      KM_Inicial = st.slider('Selecione quantos kilômetros o veículo já percorreu',value = 0, min_value= 30000, max_value= 400000, step=5000)

   col1, col2= st.columns(2)
   with col1:
      # st.header("Segmento do veículo")
      tipo_veiculo = st.radio("Segmento do veículo", 
                              ('L.A.-STRALIS 6X2','L.A.-STRALIS 6X4'))#,'L.A.-STRALIS 4X2'))

   with col2:
      missao = st.radio("Regime de uso do veículo",
                        ('Rodoviário','Severo'))

   d = {'Tipo_veiculo': [tipo_veiculo], 'Missao': [missao],'Km_inicial':[KM_Inicial],	'Km_percorrido':[KM_Anual]}
   input_data = pd.DataFrame(data=d)

   # predicao_modelo_carregado = predict_model(modelo, data = input_data)
   # predicao_modelo = modelo.predict(input_data)
   # predicao_modelo = predict_model(input_data)

   # tabela_predicao = predicao.iloc[:,3:]
   #valor_base_modelo_carregado = predicao_modelo.iloc[-1,-1]
   # Rodando o modelo com os parâmetros de entrada
   valor_base = 508.13

   #Carregando os parâmetros para precificação com modelo
   #valor_base = 0
   lista_mensalidade = []
   lista_mensalidade.append(valor_base*(1-1/np.exp(1)))
   lista_mensalidade.append(valor_base)

   for i in range(8):
      lista_mensalidade.append( lista_mensalidade[i+1]*(1+1/np.exp(1)))

   lista_mensalidade = [round(m,2) for m in lista_mensalidade]

   anos_de_contrato = 5
   KM_Anual = 200000

   lista_fator = [i*0.5 for i in range(1,2*anos_de_contrato+1)]
   lista_meses = [str(int(12*i))+'M' for i in lista_fator]
   lista_km    = [int(KM_Anual*i) for i in lista_fator]

   lista_preco_total = [round(m*12*f,2) for f,m in zip(lista_fator,lista_mensalidade)]
   lista_preco_km    = [ round(p/k,4) for p,k in zip(lista_preco_total,lista_km)]



   dados = {'Meses':lista_meses,
            'Km rodados':lista_km,
            'Mensalidade':lista_mensalidade,
            'Preço Total':lista_preco_total,
            'Preço por Km':lista_preco_km
         }
   df = pd.DataFrame(dados)
   st.table(df)

