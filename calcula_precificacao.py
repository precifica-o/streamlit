## base_pesados  = pd.read_csv(raiz_pasta + "base_analitica_pesados.csv")#, dtype = dict_dtypes, parse_dates=parse_dates)
import pandas as pd
import numpy as np

from sklearn.linear_model import LinearRegression


anos_de_contrato = 5

imposto = 9.25  # valor percentual. 
bonus_1 = 450
bonus_2 = 650
fee = 15 # valor percentual. 


lista_fator = [i*0.5 for i in range(1,2*anos_de_contrato+1)]
lista_tempo = [str(int(12*i))+'M' for i in lista_fator]


def calc_prec(base_pesados, tipo_veiculo, missao, KM_Anual):
    condicao = (base_pesados['Commercial Range Current Description'] == tipo_veiculo) & (base_pesados['MISSÃO'] == missao)
    custo_semestral = base_pesados[condicao][['Vin Code','Vehicle Km',
                                            'Approved Amount (Local)',
                                            'Preco_atualizado_falha']].copy()

    custo_semestral.rename(columns={'Vin Code':'Rótulos de Linha',
                                    'Vehicle Km':"Max. de Vehicle Km",
                                    'Approved Amount (Local)': 'Soma Approved Amount (Local)'}, inplace=True)


    for tempo,fator in zip(lista_tempo,lista_fator):
        custo_semestral[tempo] = custo_semestral['Preco_atualizado_falha']
        custo_semestral.loc[~((custo_semestral['Max. de Vehicle Km'] >= (fator-0.5)*KM_Anual) &
                            (custo_semestral['Max. de Vehicle Km'] < fator*KM_Anual)), tempo] = 0

    custo_semestral.drop('Preco_atualizado_falha', axis=1, inplace=True)


    dic_colunas_fixas = {'Max. de Vehicle Km': 'max','Soma Approved Amount (Local)':'sum'}
    dic_colunas_variaveis = {col_name:'sum' for col_name in custo_semestral.columns[3:]}
    dic_colunas_fixas.update(dic_colunas_variaveis)

    custo_semestral  = custo_semestral.groupby(by=['Rótulos de Linha']).agg(dic_colunas_fixas).reset_index()


    ####### Custos por veículo
    Qtd_Veiculos = []
    for fator in lista_fator:
        Qtd_Veiculos.append(custo_semestral[(custo_semestral['Max. de Vehicle Km'] >= (fator-0.5)*KM_Anual) ].shape[0])

    Custo_total = []
    for coluna in custo_semestral.columns[3:]:
        Custo_total.append(custo_semestral[coluna].sum())

    ## Obs: como temos a possibilidade da divisão de 0/0, inserimos a condição para qtd diferente de zero
    Custo_por_unidade = [round(custo/qtd,2) if qtd != 0 else np.nan for custo,qtd in zip(Custo_total, Qtd_Veiculos) ]

    Custo_por_km = [ round(custo/(KM_Anual/2),3) for custo in Custo_por_unidade]



    Precificacao_1 = []
    qnt_elementos = 0
    soma = 0
    for custo in [custo/(KM_Anual/2) for custo in Custo_por_unidade]:
        qnt_elementos += 1
        soma += custo
        Precificacao_1.append(soma/qnt_elementos)

    Custo_por_unidade_P1 = [fator*KM_Anual*preco for fator,preco in zip(lista_fator,Precificacao_1)]
    Precificacao_1 = [valor for valor in Precificacao_1]

    ## Regra de Correção 10%: 
    Custo_por_unidade_correcao_10 = [Custo_por_unidade_P1[0]] + [ b if b/a > 1.1 else 1.1*b for a,b  in zip(Custo_por_unidade_P1[:-1],Custo_por_unidade_P1[1:])]

    Custo_por_unidade_imposto = [preco/(1-imposto*0.01) for preco in Custo_por_unidade_correcao_10]

    ## O bonus 1 é aplicado nos 4 primeiros semestres (até index = 3). Após isso, usa o bonus 2
    Custo_por_unidade_bonus = [(custo + bonus_1) if Custo_por_unidade_imposto.index(custo) <4 else (custo + bonus_2)  for custo in Custo_por_unidade_imposto]

    Custo_por_unidade_fee = [preco/(1-fee*0.01) for preco in Custo_por_unidade_bonus]

    ### ATENCAO !!!
    # O PRECO POR KM CONSIDERA UMA REGRESAO LINEAR PARA OS CASOS ONDE NÃO Há DADOS
    # preco_por_km é nomeado com "standard" na planilha de precificação
    preco_por_km = [preco/(fator*KM_Anual) for fator,preco in zip(lista_fator,Custo_por_unidade_fee)]

    # AO NÃO SE CALCULAR A REGRESSAO LOGÍSTICA, HÁ UMA REDUDâNCIA NO CáLCULO DE PREÇO TOTAL
    # POIS ELE É O MESMO QUE Custo_por_unidade_fee

    # Custo_por_unidade_fee sem arredondamento
    list_aux = [preco/(fator*KM_Anual) for fator,preco in zip(lista_fator,Custo_por_unidade_fee)]
    preco_total_final =  [preco*(fator*KM_Anual) for fator,preco in zip(lista_fator,list_aux)]
    del list_aux

    mensalidade = [preco/(fator*12) for preco,fator in zip(preco_total_final, lista_fator)]

    preco_total_final =  [preco for preco in preco_total_final]



    #############  Tabela final de precificação
    dic_precificacao = { 'Km rodados'   : [int(fator*KM_Anual) for fator in lista_fator],
                        'Qtd de Veículos': Qtd_Veiculos,
                        'Custo por unidade': Custo_por_unidade,
                        'Custo por km' : Custo_por_km,
                        'Mensalidade' : mensalidade,
                        'Preço Total': preco_total_final,
                        'Preço por km': preco_por_km,
                        'Precificação 1': Precificacao_1,
                        'Precificação 1 - unidade': Custo_por_unidade_P1,
                        'Correção 10%': Custo_por_unidade_correcao_10,
                        'Preço com imposto': Custo_por_unidade_imposto,
                        'Preço com Bônus': Custo_por_unidade_bonus,
                        'Preço com taxa': Custo_por_unidade_fee

                        }

    tabela_precificacao = pd.DataFrame.from_dict(dic_precificacao, 
                                                columns = [str(int(fator*12))+'M' for fator in lista_fator], orient='index')

    # Usaremos a transposta para facilitar a leitura em casos de muitos anos de simulação
    tabela_precificacao = tabela_precificacao.T
    tabela_precificacao = tabela_precificacao.astype({"Km rodados":'int32', "Qtd de Veículos":'int32'})


    ### Etapa de regressão linear
    # Usado nos casos onde não há histórico de dados
    # É feita um projeção de valores para as colunas 'Preço por Km' e, a partir daí, são calculados os valores
    # para 'Mensalidade' e 'Preço Total'

    # n: numero de valores nao_nulos para 'Preço por km'
    n = tabela_precificacao[tabela_precificacao['Preço por km'].notna()].shape[0]
    m = anos_de_contrato*2

    # vetor x para regressão
    x_v = tabela_precificacao[['Km rodados']]
    lista_km = [int(fator*KM_Anual) for fator in lista_fator]

    for i in range(n,m):
        # vetor y para regressão
        y_v = tabela_precificacao[['Preço por km']][0:i]

        # criando e treinando o modelo
        model = LinearRegression()
        model.fit(x_v[0:i], y_v)

        predicao = model.predict(x_v[i:i+1])

        #Populando a tabela de precificação com os dados preditos
        condition = tabela_precificacao['Km rodados'] == lista_km[i]
        tabela_precificacao.loc[condition , 'Preço por km'] = predicao
        tabela_precificacao.loc[condition , 'Preço Total'] = predicao*lista_km[i]
        tabela_precificacao.loc[condition , 'Mensalidade'] = predicao*lista_km[i]/((i+1)*6)

    tabela_precificacao.fillna('--', inplace = True)
    tabela_precificacao[['Preço Total', 'Mensalidade']] = tabela_precificacao[[ 'Preço Total', 'Mensalidade']].round(1).astype(str)
    tabela_precificacao[['Preço por km']] = tabela_precificacao[['Preço por km']].round(4)

    tabela_precificacao[['Custo por unidade','Custo por km']] = tabela_precificacao[['Custo por unidade','Custo por km']].astype(str)

    tabela_precificacao = tabela_precificacao.iloc[:,:7]
    #ocultando colunas sensíveis
    #tabela_precificacao = tabela_precificacao.iloc[:,[0,4,5,6]] 

    tabela_precificacao.index_name = 'Meses'
    return tabela_precificacao