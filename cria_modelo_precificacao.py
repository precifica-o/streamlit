from pycaret.regression import *
import pandas as pd

 #scikit-learn==0.23.2
from pycaret.utils import enable_colab

def cria_modelo():
    base = pd.read_csv("base_analitica_precificacao.csv")

    base = base[['Tipo_veiculo', 'Missao', 'Km_inicial', 'Km_percorrido','Gasto_mensal_falha']]

    condition = (base['Gasto_mensal_falha'] < 20000)
    s = setup(data = base[condition], target = 'Gasto_mensal_falha', session_id=42)
    modelo = create_model('rf')

    return modelo